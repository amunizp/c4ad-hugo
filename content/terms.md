+++
title = "Terms"
id = "about"
+++


# FLOSS service

Payment expected in net30 days.

All IP created is property of Andres Muniz-Piniella

All IP created will be released under 
CC-BY-SA 4.0, GPL v3 or later versions
of similar licences. 

No warranty. No liability. 

Costumers will allow C4AD to publicise
the client and any IP created by C4AD

Confidential information, C4AD will do 
it's utmost to keep information secret but 
will recommend clients not to transfer any 
confidential information to C4AD  if there
are concerns. This is in part due to the 
technical know how needed to ensure proper information containment. 

If there is a force majeure both parties will 
be understanding of the circumstances. 

Software will be released under GPL v3 or similar 
free culture licence.

Reports, hardware schematics, etc will be 
released under CC-BY-SA 4.0 or similar 
free culture licence. 

# Licence
cc-by-SA
# Versions

2018-03-04 Reviewed by Andres Muniz-Piniella
2018-06-13 Reviewed by Andres Muniz-Piniella
2019-10-21 Reviewed by Andres Muniz-Piniella

