+++
date = "2018-03-01T12:00:00-00:00"
title = "STEAM punky"
draft = "false"
tags = ["Soft Skills", "community", "team building"]
toc = "true"
author = "Andres"
+++



Calling all makers looking to change the world! We are looking for you to join us in this adventure. 

{{< figure src="../STEAMpunkyImg/STEAM-PUNKY.pdf.png" height= "auto" width="900px" >}}

S.T.E.A.M. : Stands for Science, Technology, Engineering, Arts and Maths. Using those tools we can change the world!

And punky? Well that is YOU!

## How will it work

We get a group of six punky and first we discuss what problem we want to solve together. There is no such thing as a bad idea, we dump all the different things we want to do. 

{{< figure src="../STEAMpunkyImg/PUNKY-program.jpg" height= "300" width="auto" >}}


We then funnel the ideas until we decide on something we really want to do as a TEAM. After that it is time for a bit of research of looking for giants to stand on their shoulders. We can't find them? Then we will have to be the first giants! We then get making!

{{< figure src="../STEAMpunkyImg/PUNKY-make.jpg" height= "300" width="auto" >}}


So what happens with what we build, who keeps it? Everything will be free, as in liberated, and we will publish our designs online for others to enjoy. Also, if further production is needed, that is something that can be explored as well, maybe via the [tps coop](http://www.tps.coop/)!

## How can we achieve this? 

For the first few sessions there will be a lot of thinking and brain storming in groups and discussing. It will help as a getting to know the other Punky.

For the first part post-its, pens, colours will be enough. For the last part some minor researching online will be used but one device will be enough we will search for key words together and take notes. 

The making is trickier. Depending on what the project is we would have to order the equipment in preparation for the next week. We will have to be very careful with what we order! It might mean doing something cool or having to go back to the drawing board. Some equipment we can loan to reduce this risk.  

 
