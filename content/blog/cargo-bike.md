---
title: "Cargo Bike"
date: 2021-04-12T02:01:16+01:00
draft: false
tags: ["community", "Ham", "bike", "local", "TW10"]
toc: true
author: Andres
---

 
# Would you be interested in a Parkleys cargo bike share scheme?
Richmond Council is asking for bids for their Active Richmond Fund and we are gauging your interest in PRS making a submission to set up an electric cargo bike share scheme for residents.  

Cargo bikes are bikes with a container attached.  An electric bike still keeps you fit but makes light work of heavy loads and hills, making it accessible to a wider range of people.  The bid would include funding for the bike, secure storage and setting up an online booking system.  It may include offering some match funding, in the form of residents time (rather than money) setting the scheme up and maintenance.

We would appreciate your feedback.

{{< rawhtml >}}
<div>
<style>.dataframe th{
background: rgb(255,255,255);
background: radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(236,236,236,1) 100%);
padding: 5px;
color: #343434;
font-family: monospace;
font-size: 110%;
border:2px solid #e0e0e0;
text-align:left !important;
}.dataframe{border: 3px solid #ffebeb !important;}</style>
</div>
 {{< /rawhtml >}}




```python
import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv("CargoBike_(1-8).csv")
pd.set_option('display.max_rows', None)
pd.set_option('display.max_colwidth', -1)
```

## Would you be interested in using a cargo bike if it was available?


```python
Q1=df['Would you be interested in using a cargo bike if it was available?'].value_counts()
Q1.plot.pie()
plt.ylabel('')
plt.title('Question 1');
```

{{< figure src="../BikeReport/output_3_0.png" >}}



## If you answered yes, what would you use if for?  Select as many as you want.



```python
col_name2="If you answered yes what would you use if for?  Select as many as you want."
df.rename(columns={col_name2:'Q_2'}, inplace=True)
q2_df = pd.DataFrame(df['Q_2'])
Q2_df=q2_df.assign(Q_2=q2_df.Q_2.str.split(';')).explode('Q_2')
Q2=Q2_df['Q_2'].value_counts()
Q2.plot.pie()
plt.ylabel('')
plt.title('Question 2');
```

{{< figure src="../BikeReport/output_5_0.png" >}}



## If other what would you use if for?



```python
Q3=pd.DataFrame(df["If other what would you use if for?"]).dropna()
Q3
```



{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>If other what would you use if for?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Picnics. moving things around the borough.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>package delivery for possible business.</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Kids maybe and big shop items eg plants</td>
    </tr>
    <tr>
      <th>4</th>
      <td>allotment</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Garden centre stuff</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Carrying things to and from my allotment Also picnics!</td>
    </tr>
  </tbody>
</table>
</div>
{{< /rawhtml >}}


## Would you be happy to subscribe a small amount to pay for insurance and repairs?



```python
Q4=df['Would you be happy to subscribe a small amount to pay for insurance and repairs?'].value_counts()
Q4.plot.pie()
plt.ylabel('')
plt.title('Question 4');
```

{{< figure src="../BikeReport/output_9_0.png" >}}



## Would you be happy to book the bike via an online system?


```python
Q5=df['Would you be happy to book the bike via an online system?'].value_counts()
Q5.plot.pie()
plt.ylabel('')
plt.title('Question 5');
```

{{< figure src="../BikeReport/output_11_0.png" >}}



## How frequently would you use a cargo bike?


```python
Q6=df['How frequently would you use a cargo bike?'].value_counts()
Q6.plot.pie()
plt.ylabel('')
plt.title('Question 6');
```

{{< figure src="../BikeReport/output_13_0.png" >}}



## How far would you travel? In Miles


```python
Q7=df['Points - How far would you travel? Miles'].value_counts()
Q7.plot.bar()
plt.ylabel('Number of responses')
plt.xlabel('Miles')
plt.title('Question 7');
```

{{< figure src="../BikeReport/output_15_0.png" >}}



## Which form of transport do you currently use, that the bike would replace?


```python
Q8=df['Which form of transport do you currently use that the bike would replace?'].value_counts()
Q8.plot.pie()
plt.ylabel('')
plt.title('Question 8');
```

{{< figure src="../BikeReport/output_17_0.png" >}}



## Would you be interested in joining a community activity to build the secure storage shed and/or the bike?


```python
Q9=df['Would you be interested in joining a community activity to build the secure storage shed and/or the bike?'].value_counts()
Q9.plot.pie()
plt.ylabel('')
plt.title('Question 9');
```

{{< figure src="../BikeReport/output_19_0.png" >}}



## The shed might be used to store other bikes and have space for tools etc.   Select any of the below that you agree with.


```python
Q10=df['Select any of the below that you agree with.'].value_counts()
Q10.plot.pie()
plt.ylabel('')
plt.title('Question 10');
```

{{< figure src="../BikeReport/output_21_0.png" >}}


