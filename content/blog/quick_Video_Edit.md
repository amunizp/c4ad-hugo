---
title: "Quick_Video_Edit"
date: 2020-09-11T17:04:58+01:00
draft: false
---

So if you want to compress a video that is just too big to upload or 
email you might be OK with compressing it. And I mean compressing the actual
vido not putting it in a zip. 

```
$ ffmpeg -i OriginalVideo.mp4 OriginalVideoCompressed.mp4
```

I had a 1.2 Gb file that turned into 0.9Mb. But maybe before you do that
you can just remove bits that you do not need in the first place. 
Like getting ready part of your video or the "cut" part at the end. 

```
$ffmpeg -ss 00:19:00 -i originalVideo.mp4 -t 01:32:00 -c copy outVideo.mp4
```

The cutting part takes seconds the compressing can take a full hour (or hours!). 
 

How about creating a video from a list of images in a folder?
```
cat *.jpg | ffmpeg -f image2pipe -r 1 -vcodec mjpeg -i - -vcodec libx264 mateo.mp4
```


