---
title: "Web Pages PortFolio"
date: 2024-07-19T10:14:58+01:00
draft: false
author : "Andres"
tags :  ["HTML", "JS", "free"]
---

I have made several web pages over the years I don't think I can remember/recover them all. 

But here is a selection of what I recall. Unless otherwise stated assume they are defunct.



## Free hosting via gitlab pages:
* https://c4ad_cic.gitlab.io/sheddington-web/ Landing page for Sheddington. This was temporary and they now have a nice CMS system which they can manage themselves. 
* https://richmondmakerlabs.gitlab.io/plain-html/ was a colaboration project to do mainly Jouletheif kits so that people had cheap sources of lights to feel safe when it gets dark. Sadly never cristalised. https://gitlab.com/RichmondMakerLabs/plain-html The code can be found here: 
* https://tps-coop.gitlab.io/plain-html/ was the original start from https://www.crystalisr.coop/ from when it had a different name. Here you can see the code for it: 
* https://tps-coop.gitlab.io/growth-pathway was a project for tps (as above) a way to recruit for a learning platform we were setting up. You can find the code here:  
* **Active**: https://gitlab.com/amunizp/c4ad-hugo where you can see the code for this website.

## Free hosting via netify and gitlab:
* A static page for a co-working space where I took over from https://polynomial.se/ it is now managed by someone else with a WP CMS: https://canburyworks.co.uk/ the original code can still be found here: https://gitlab.com/canburyworks/canburyworks-2017


## Free Hosting via netify and github mostly rock the Code learning:
* **Active** CV profile page might add more things to it later. https://app.netlify.com/sites/amunizp-cv/ code can be found here. 
 some easter eggs can be found but other than that, simple page. 
* **Active** Simple games done in JS vanilla https://rincondejuegos.netlify.app/ the code is hosted here: https://github.com/amunizp/RockTheCode-proyecto5-juegos
* **Active** new richmond makerlabs page: https://jade-kitsune-7638fb.netlify.app/ waiting to be deployed in https://www.richmondmakerlabs.uk/
code can be found here: https://github.com/amunizp/webpage 

