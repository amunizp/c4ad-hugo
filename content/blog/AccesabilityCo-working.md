+++
date = "2017-06-13T12:00:00-00:00"
title = "Accessable CRE-8 and Canbury Works"
draft = "False"
tags = ["accessible", "co-working", "testing"]
toc = "true"
author = "Andres"
+++

We applied for funding from Mayor of London. The funding was to enabling companies to have access to testing facilities and making the space more accessible. What follows are some of the notes taken in the research to find out exactly what is needed. 


#TODO
* write up document **Action** 
* We want to do a Make and Test facility available to all. 
* We need a chair to make it accessible to go up the stairs. Or other accessibility. <b>Action</b>: Andres Talk to Demand.org.uk DONE 
* We would want to create a learning introduction to product development including testing. **Action**: Use de-risk and all you need to know about production. "why start ups fail" diagram". Reach goal will be that  Achilleas courses will be available down the line. DONE
* The course will need demonstrators which we will need to purchase. This is a reach goal. <b>Action</b> Andres to budget for them. DONE
* Link up with NEA as a partner sponsor **Action** David to call them up for Business development. http://www.ixionholdings.com/call-for-partners 01245 505648 DONE

#Test equipment guide
http://www.emcfastpass.com/emc-test-equipment-guide/


#Disability notes

* Lynette from Demand.org.uk has given some input on an email.
## Philip J Connolly Policy and Development Manager, Disability Rights UK
* There are different types of disabilities:
    * Physical disability which affects in one way or other to 15% of the population
    * Sensory impairment: would need to adjust for digital displays room indicators, etc.
    * Mental Health which is a growing trend in Young people: depression, anxiety. So the idea is to help have a better ambient
    * Neurodiverse: autism, dyspraxia, and so on are disabilities in learning. In a workshop environment is seeing a risk and understanding the risk. [Andres Idea: using emojis?]
    * Hidden disabilities: epilepsy, long term medial conditions, cancer, diabetes,... these involve the tutors having some guidance as to what to do. 
* It is important to get a focus group in to assess what is needed. Suggest going to local charities to find out. 
* Everything is solvable it is just easier in the outset. 
* Some research in Greater Manchester: Solfort. contact is Nick Taylor of Dundee University. ["in the Making" BBC video](http://www.bbc.co.uk/news/av/technology-33780101/3d-printing-a-disability-revolution) 
* Sometimes all that is needed is to make everybody aware of the issues: say someone that suddenly yells out words would make a class disruptive but making it known in the outset to everybody makes people comfortable with it. 
* In all induction make a reference to different disabilities. 
* Some times all it takes is to think slightly differently: rig a light to a doorbell and a deaf person can now open the door. 
* Everything is easier on the outset. 
* The Department of work and pensions wrote a green paper "improving peoples lives" this will hopefully release an "innovation fund". Due to come out soon (2017).
* Always think of alternative ways of doing things.  What could happen is someone can only engage with a machine for 1hr and later need to take a break because the pain is too much. It is about letting this person have access to the machine 1 hr later. 
* It is very helpful to have a local Reference group. 
* Also give descriptions of the location of public transport: trains, buses, taxi,...
* Contacted ["Kingston Centre for Independent Living" CEO Lisa Ehlers](http://www.kcil.org.uk/page/whos-who-kcil)
## Kingston centre of Independent Living
* Meet up on Monday the 5th to discuss the the needs:
* They can provide assessment on:
    * Physical
    * Visual impairment
    * Young people with Asperger's
### Face to face meeting
* We met with Robert and Lisa. 
* we have to review legal document called approved Document M. Particularly section 4 that covers events space.
* Key things noticed straight away: 
    *  hand rail needs to be far away from wall as cable is currently in the way of hands.
    *  Extra hand rail will help disabilities and advanced age.
    *  Using colour or even pictures hanging on the wall is better than plain white walls as it helps with visual impairment.
    *  We can borrow the hearing aid loop system from them. 
* Long term solution can be using the bottom floor as the event space. 
* A projection of the space down stairs is not too difficult. Also use microphone to talk to upstairs. 
* Tables with catering/ bars should be low enough. 
* Accessible leaflets (large print plain colour) OR simply provide written documentation ahead of the presentation will help follow the presentation. 
*  Arial font 12-14
*  Flower pot in bathroom is in the way of the wheel chair movement.
*  It would be good to write up the long term plan for the space. 
*  It might be a good idea to ask the landlord to help with making the space accessible. 
*  Development trust is definitely a way to go.
*  We can get the endorsement of local MP and councillors
*  KVA is a good group to send the link to promote the crowd funding. 
*  Agreed to wait until after 12-13 of June to advertise crowd funding.
*  A-frame on the curb is illegal in Kingston. 





#Notes 
* See attached PDF page 32 for ideas of what to do.
* Key words: Sustainability, IoT, skills, high tech, start-ups, TRL3 to TRL6, online learning combined with physical use, women in tech opening event, Stopwith, hawker
* What do we want? Equipment? Pay the lease? Set up the cre-8.org CIC? or more like south west london newtech. 
* Be able to offer people off the street some training on equipment and later usage (not using the office space). Sponsored membership for people in job Seekers allowance? (too complicated?)
* Can we ask digital moms to help?
* Canbury works and LoC to sponsor through the event?
* Contact Old Kent Road studios to ask for help? http://liveseyexchange.com/
1. How good is the idea? empower people and grow prosperity
2. Is your project ready to go? We can't say we are ready to go now in the certain aspect that we are asking but we can say we will be ready to go on what ever we ask for once we get the funding.
3. Long term benefits? Incubation centre, seeder, accelerator? Why will we work were others have failed?
4. Strength in support. Who is our local community? all the people that have to commute to silicon roundabout?





 
