+++
date = "2017-09-01T12:00:00-00:00"
title = "CNC machining of a cup"
draft = "False"
tags = ["CAD", "CNC", "Art"]
toc = "true"
author = "Andres"
+++

 
How to machine a cup with a 3 axis CNC machine?

Well, one of the first things you will need is the CAD data. This needs to be drawn with minimal undercuts. Parametric design is ideal here to be able to use different proportions: say you want to start with a cup, then move to a shot glass or a bowl for soup. 

Second is a system to be able to align the features exactly to the same place when flipping it into different directions on your CNC. 

**Third** when machining the item you want to make sure that you do not cut it loose: you will need to either have a tool path generator that will automatically do support structure, a CAD package that will do it for you or manually draw it yourself.  

 
