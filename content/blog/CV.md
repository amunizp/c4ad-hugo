---
title: "CV"
date: 2024-01-12T09:52:34Z
draft: false
tags: ["CV", "iframe", "html", "googleDocs"]
toc: "false"
author: "Andres"
---




This is a test to see if I can embed my spanish CV into a blog post. By simply embedding the iframe as directed by google docs "publish to web" feature. 

<iframe src="https://docs.google.com/document/d/e/2PACX-1vSwb_GrPmSX_HYQcdHGt6ej9bb8EN7QvV93CFxDiHWiApTwYeVjy0rYa-d4RHJQzw/pub?embedded=true" 
  title = "Andres CV linked from google doc"
></iframe>

you can add this to the iframe  height="600px" width="100%" and so forth or simply create an iframe.css and call it from the header. 
