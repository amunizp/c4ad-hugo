+++
date = "2018-02-01T12:00:00-00:00"
title ="Map"
author = "Andres"
tags = ["OSM", "community", "map"]
toc = "false"
+++


These are the locations where you can reach me. 

Registered address in Teddington. Community address in Ham. And business address in Kingston. 

{{< openstreetmap mapName="c4ad-cic_240326" scale="14"  >}}

This gives me a perfect opportunity to talk about a job. There is a small [wood land](https://www.richmond.gov.uk/services/parks_and_open_spaces/find_a_park/petersham_common_woods) called Petersham Common Woods. They are managed by a community group that needed to have their trees documented i.e. tagged. They found that software developed for this task tagging and geolocation in a map. Is normally proprietary and only makes sense for large forests. The cost is also prohibitive with a yearly licence fee. 

The alternative I offered was to use Open street Maps. They could either keep the tagged trees with their geolocation on a local computer or to avoid the Bus Scenario* they could upload it to the open street map servers. The latter is very useful to provide long life to the project. 

So what I did was write some documentation so that we could have a process in place in order to do this. This is now hosted in the [OSM](https://wiki.openstreetmap.org/wiki/Richmond_Upon_Thames,_UK/Petersham_Common_Woods) and I encourage anybody that like woodlands and local comunity to help out! Looking after woodlands is a joint effort.  

*By Bus Scenario I mean what happens when the owner of a project or the person who keeps all in their mind is suddenly hit by a bus (or something just as unlikely). 
