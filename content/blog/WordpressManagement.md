+++
date = "2017-09-05T12:00:00-00:00"
title = "word press: lessons learnt"
draft = "False"
tags = ["wordpress"]
toc = "true"
author = "Andres"
+++

I had dipped my toes into wordpress Content management System (CMS) before but for CRE-8 I have gone into selecting a theme and making sure it was a bit more mobile friendly. 

My conclusion is that wordpress is a great tool. Being able edit conent online and having a version control system which you can roll back to history is great. What follows are some of the lessons I learnt about doing a CMS.

* If you are hosting it you will need to keep atop of the server updates such as the PHP updates. Most popular server hosts will promptly alert you if needed. 
* When copying and pasting content to be careful as the font and other typsetting features will be copied. This is good for section headers and such but can be a bit of a pain if you copy the font as well: you might end up with non-uniform looking website. 
* Themes and plugins are great but there is so many options it is difficult to choose. My rule of thumb is that if I want a feature rich theme or plugin I will look to those that have more reviews and users. But If I just want a simple toold I will look for either the most recently modified or do some web search in order to find the right tool for the jub with minimal overheads. 
* Companies sometimes just want some static content. They will edit it once every blue moon and even though it is a CMS with what you see is what you get text editors it can be a bit daughting to relearn where you have to click to edit. Especially if new themes are used. Hence even if you give your costumer a review of how to do these you will probably get repeat business. So might as well give them all the information you can, so that they remember you!
 



 
