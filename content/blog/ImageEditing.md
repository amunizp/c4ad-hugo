+++
date = "2018-08-01T12:00:00-00:00"
title ="Batch image and pdf edits"
author = "Andres"
tags = ["command line", "image", "pdf", "batch"]
toc = "true"
+++

## Doing some image editing on the command line

For my reports, this web page and for personal sharing via social media I have used several commands. Why use commands when there are apps, and programs that will do it? Well, I could come up with all sorts of reasons: batch processing, more control (like driving stick), repeatability and so on. But in truth is that I enjoy finding out, and dislike the imprecision of using a mouse. Also it is very probable that most of the apps are using Imagemagick already so it is interesting to go to the source.  

The following commands were done on an ubuntu 16.04 or 18.04. Programs like [ImageMagick](https://imagemagick.org/script/index.php) will need to be installed. If you don't have one of these programs installed ubuntu will prompt you to install the correct program (isn't that helpful of an operating system!)

### Do thumbnails


`convert -define jpeg:size=2000x2000 1.jpg  -thumbnail 426x213^ -gravity center -extent 426x213  1.jpg`

or

`convert -define jpeg:size=2000x2000 DeanOriginal.jpg  -thumbnail 426x426^ -gravity north -extent 426x426  1.jpg`

The first bit is to make them all as big as the biggest (in this case 2000 px wide?)
Then make it as wide as the smallest. 
It crops to the centre. 

`convert -rotate "90" 6.jpg 6.jpg`

Detail for those really interested  [source](https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/)

### Doing a montage
`convert -define jpeg:size=2000x2000 IMG_*.jpg  -thumbnail 426x213^ -gravity center -extent 426x213  Thumb_*.jpg`
Convert all the items that start with IMG and end with jpg to to small centred image file (thumbnail) named Thumb at the begining and extension jpg. 

`montage Thumb_-*.jpg -geometry +1+1  -background lightgreen GreenLabs.jpg`

Then stitch them together with a bit of spacing and leave a light green back ground. 

### Do water mark 


`composite -watermark 30% -gravity southeast             C4AD-CC-BY-SA.svg  FirstFloorView3-d.png    FirstFloorView3.png`

Put the first image on the south east (bottom right) of the second image. I used SVG with a little bit of transparency here. 

`composite -quality 100 -density 400 -dissolve 70% -gravity north -background none  Logo8-Grey.svg  headerWater.jpg headerWater9.jpg`

Here I let image magic do the transparency. 



### other image changes


* Blur edges to transparency: `convert wrapper-img.png -alpha set -virtual-pixel transparent -channel A -radial-blur 0x100 +channel  blurwhite.png `
* Resize according to width  `convert FlyerW.jpg -resize 532 FlyerW.png `
* Reduce size of edge:  `convert -strip -interlace Plane -gaussian-blur 0.05 -quality 60% FlyerW.png result.png`
* Crop an image `convert img_eggsif_8474955069117216231.jpg -crop 1944x1944+0+100 flyer.png`
* Make greyscale: `convert white.jpg -type Grayscale BandW.jpg`
* Flip image left to right: `convert slider-bg.jpg -flop slider-bgInv.jpg` 
* Take all the images and bundle them into a pdf document. `$ convert Pix*.png Pixton.pdf`
* Making smaller size: `convert SolderedFinish.jpg -resize 200 SolderedFinish_small.jpg`
* Changing transparent background to white: `convert favicon.ico  -background white -alpha remove white.ico` (this is useful for desktop Icons. 
* Changing image size to a minimum and replacing the existing images: `mogrify -resize "200>"  *.*`
* When you take the picture with the wrong orientation on your camera:  `jhead -autorot Jurassic.jpg` 


##  PDF tools

* merge several PDF files into one `pdfunite in-1.pdf in-2.pdf in-n.pdf out.pdf`
* Compressing PDF: [Source](https://stackoverflow.com/questions/5296667/pdftk-compression-option)
* Compressing PDF (2): gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default \
    -dQUIET -dDetectDuplicateImages \
    -dCompressFonts=true -r150 -o output.pdf input.pdf
* Convert pdf to png (when image magick version is less than 7): `gs -dBATCH -dNOPAUSE -sDEVICE=pnggray -r300 -dUseCropBox -sOutputFile=item-%03d.png examples.pdf`




