---
title: "Tfl BusCountDown"
date: 2022-07-10T11:26:45+01:00
author: "Andres"
tags: ["CSS", "HTML5", "API", "tfl"]
toc: "true"
---

# Tfl Bus Count Down
## TL;DR

1) Go to https://gitlab.com/RichmondMakerLabs/JS-countdown 
2) Download code, or git clone it to your computer
3) Go to tfl and create your own API key. https://api-portal.tfl.gov.uk/signin
4) Edit the file tfl-api-key-template.js remove -template from the name of the file and put your ID and key inside it. 
5) Check that the page works by opening index.html
6) realise that if you upload this to a public webserver someone might on purpose or not not purpose copy your tfl key and and api key. Trust people and do it anyway. 
7) Copy the code to your favourite web hosting like bitfolk!
8) Mobile! You can put all the CSS into the <style></style> tag and all the JS script into the <script></script> tag so that it is all in one index.html page. You can then share this as a document and your friends will be able to open it on their mobile browsers. (*)

(*)  I have noticed that this does not work with firefox (which I think is reasonable, as a user should not be albe to run JS on your phone locally as instictively it might have access to your data? But Chrome allows it. you might need to have a file browser to open it again. I use midnight commander https://f-droid.org/en/packages/com.ghostsq.commander/

## Why?

People can check their apps really quickly, short cut in their apps to make local bus stop prominent. Steps are: take up phone, select app, check your nearest stop. This might be: take out phone & click on web page hotlink.  Or look up and see the Kiosk.

So this is really only useful for frequently used bus stops.  

Say you live in a block of buildings.  

But, you could have a QR code posted on your door that you can scan and a web page with your next bus coming really quickly. 

Or have an information kiosk that is always on. 

Or you have two busses that will take you where you want to be but they are in two different bus stops. 



 



