---
title: "CompanyListing"
date: 2021-04-20T20:34:55+01:00
draft: false
tags: ["community", "Ham", "networking", "local", "TW10"]
toc: true
author: Andres
---

# Local Company Finder UK
Objective, find an up to date list of companies from .gov website also find coops from FCA website. Filter by the first half of the post code For example if you live in IG11 0HQ find the jobs in IG11 in UK this is normally a ward. 

## Who might be interested? 

* Local counciours to know what companies are in their ward.
* Job seekers looking for work. 
* Buyers who wish to buy local. 
* Companies finding local providers to reduce carbon footprint on transport. 
* Companies looking for clients in other companies. 

## First test

Companies house create a CSV file that it updates every month. This comes in a Zip that is downloaded from their website. This is full nationwide. 
http://download.companieshouse.gov.uk/en_output.html


FCA seems to have an API to enable finding companies. Their website allows me to search by postcode but only finance companies: mortgage advisors and so forth. 

https://www.fca.org.uk/firms/financial-services-register
https://register.fca.org.uk/Developer/ShAPI_LoginPage?ec=302&startURL=%2FDeveloper%2Fs%2F

Coop uk also provides a useful source of information for cooperatives only Seems to collect from both companies house and FCA: 

https://www.uk.coop/resources/open-data


## Ideal vision

Hosted website, you put your post code and it gives you a downloadable CSV. Maybe an extra filter. This will help develop the 15 min city. 



```python
import pandas as pd
from zipfile import ZipFile
import glob
import os
from datetime import date
import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
```

    /usr/lib/python3/dist-packages/requests/__init__.py:89: RequestsDependencyWarning: urllib3 (1.26.4) or chardet (3.0.4) doesn't match a supported version!
      warnings.warn("urllib3 ({}) or chardet ({}) doesn't match a supported "



```python
def check_latest_online():
    PageName = "http://download.companieshouse.gov.uk/en_output.html"
    RootPageName = "http://download.companieshouse.gov.uk/"
    page = requests.get(PageName)
    soup = BeautifulSoup(page.content, 'html.parser')
    for one_a_tag in soup.findAll('a'):  #'a' tags are for links
        link = one_a_tag['href']
        if "AsOneFile" in link:
            latest_online=link
            time.sleep(1) #pause the code for a sec
            break
        else:
            next         
    return latest_online
```


```python
# Make a request
def download_companyFile():
    PageName = "http://download.companieshouse.gov.uk/en_output.html"
    RootPageName = "http://download.companieshouse.gov.uk/"
    page = requests.get(PageName)
    
    soup = BeautifulSoup(page.content, 'html.parser')

    for one_a_tag in soup.findAll('a'):  #'a' tags are for links
        link = one_a_tag['href']
        if "AsOneFile" in link:
            urllib.request.urlretrieve(RootPageName+link, './data/'+link) #this might be out of date?
            print(RootPageName+link)            
            time.sleep(1) #pause the code for a sec
            break
        else:
            next     
```


```python
def extract_latest_zip():
    list_of_ZIP = glob.glob(path_to_ZIP) 
    latest_ZIP = max(list_of_ZIP, key=os.path.getctime)
    with ZipFile(latest_ZIP, 'r') as zipObj:
        # Extract all the contents of zip file in current directory
        zipObj.extractall(path='./data/')
    list_of_CSV = glob.glob(path_to_CSV)
    latest_CSV = max(list_of_CSV, key=os.path.getctime)
    return latest_CSV
```


```python
def latest_zip_csv ():
    list_of_ZIP = glob.glob(path_to_ZIP) 
    latest_ZIP = max(list_of_ZIP, key=os.path.getctime)
    list_of_CSV = glob.glob(path_to_CSV)
    latest_CSV = max(list_of_CSV, key=os.path.getctime)
    return latest_ZIP, latest_CSV

```


```python
path_to_ZIP='./data/*.zip' # * means all if need specific format then *.csv
path_to_CSV='./data/*.csv'
path_to_data_folder='./data'
latest_ZIP, latest_CSV = latest_zip_csv ()
#latest_online_zip = check_latest_online()
#Offline work.
latest_online_zip = os.path.basename(latest_ZIP)
# Rear word replace in String using split() + join()
new_extension= ".csv"
latest_online_csv =  "".join(latest_online_zip.split('.')[:-1] + [new_extension])

if latest_online_csv == os.path.basename(latest_CSV):
    print (latest_online_csv)
    print (latest_CSV)
    print ("We have up to date CSV")
    print ("It is now business time: the latest csv is "+ latest_CSV)    
elif latest_online_zip == os.path.basename(latest_ZIP):
    print (latest_online_zip)
    print (latest_ZIP)
    print ("Latest zip downloaded but we did not extract!, will do so now.")
    extract_latest_zip()
    latest_ZIP, latest_CSV = latest_zip_csv ()
    print ("It is now business time: the latest csv is "+ latest_CSV)
else:
    print ("all fail")
    print (latest_online_zip)
    print (latest_ZIP) 
    print (latest_online_csv)
    print (latest_CSV)
    print ("So we download the newest version, please wait for 0.5 GB file to download")
    download_companyFile()
    latest_CSV = extract_latest_zip()
    print ("It is now business time: the latest csv is "+ latest_CSV)

```

    BasicCompanyDataAsOneFile-2021-04-01.csv
    ./data/BasicCompanyDataAsOneFile-2021-04-01.csv
    We have up to date CSV
    It is now business time: the latest csv is ./data/BasicCompanyDataAsOneFile-2021-04-01.csv



# What does the csv have? 

**$ head BasicCompanyDataAsOneFile-2021-01-01.csv**

CompanyName, CompanyNumber,RegAddress.CareOf,RegAddress.POBox,
RegAddress.AddressLine1, RegAddress.AddressLine2,RegAddress.PostTown,
RegAddress.County,RegAddress.Country,RegAddress.PostCode,
CompanyCategory,CompanyStatus,CountryOfOrigin,DissolutionDate,
IncorporationDate,Accounts.AccountRefDay,Accounts.AccountRefMonth,
Accounts.NextDueDate,Accounts.LastMadeUpDate,Accounts.AccountCategory,
Returns.NextDueDate,Returns.LastMadeUpDate,Mortgages.NumMortCharges,
Mortgages.NumMortOutstanding,Mortgages.NumMortPartSatisfied,
Mortgages.NumMortSatisfied,SICCode.SicText_1,SICCode.SicText_2,
SICCode.SicText_3,SICCode.SicText_4,LimitedPartnerships.NumGenPartners,
LimitedPartnerships.NumLimPartners,URI,PreviousName_1.CONDATE, 
PreviousName_1.CompanyName, PreviousName_2.CONDATE, PreviousName_2.CompanyName,
PreviousName_3.CONDATE, PreviousName_3.CompanyName,PreviousName_4.CONDATE, 
PreviousName_4.CompanyName,PreviousName_5.CONDATE, PreviousName_5.CompanyName,
PreviousName_6.CONDATE, PreviousName_6.CompanyName,PreviousName_7.CONDATE, 
PreviousName_7.CompanyName,PreviousName_8.CONDATE, PreviousName_8.CompanyName,
PreviousName_9.CONDATE, PreviousName_9.CompanyName,PreviousName_10.CONDATE, 
PreviousName_10.CompanyName,ConfStmtNextDueDate, ConfStmtLastMadeUpDate

for some reason it does not know what company number or regaddress line 2 is. 

Let us view the top 4 values of only the columns we are interested:



```python

companies_df = pd.read_csv(latest_CSV,# index_col=0, header= TRUE ,# row.names =1, 
                        usecols=["CompanyName","RegAddress.AddressLine1",
                                 "RegAddress.PostTown",
                                 "RegAddress.PostCode",
                                 "CompanyCategory" ,"CompanyStatus", 
                                 "SICCode.SicText_1", "SICCode.SicText_2",
                                "SICCode.SicText_3", "SICCode.SicText_4"])
companies_df[0:4]

```


{{< rawhtml >}}
<div>
<style>.dataframe th{
background: rgb(255,255,255);
background: radial-gradient(circle, rgba(255,255,255,1) 0%, rgba(236,236,236,1) 100%);
padding: 5px;
color: #343434;
font-family: monospace;
font-size: 110%;
border:2px solid #e0e0e0;
text-align:left !important;
}.dataframe{border: 3px solid #ffebeb !important;}</style>
</div>
 {{< /rawhtml >}}

{{< rawhtml >}}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>! LIMITED</td>
      <td>UNIT 3 NEWTON BUSINESS CENTRE</td>
      <td>SHEFFIELD</td>
      <td>S35 2PH</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>78300 - Human resources provision and manageme...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>! LTD</td>
      <td>METROHOUSE 57 PEPPER ROAD</td>
      <td>LEEDS</td>
      <td>LS10 2RU</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>99999 - Dormant Company</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>!? LTD</td>
      <td>THE STUDIO HATHERLOW HOUSE</td>
      <td>ROMILEY</td>
      <td>SK6 3DY</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>47710 - Retail sale of clothing in specialised...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>!BIG IMPACT GRAPHICS LIMITED</td>
      <td>372 OLD STREET</td>
      <td>LONDON</td>
      <td>EC1V 9LT</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>18129 - Printing n.e.c.</td>
      <td>59112 - Video production activities</td>
      <td>63120 - Web portals</td>
      <td>74201 - Portrait photographic activities</td>
    </tr>
  </tbody>
</table>
</div>

{{< /rawhtml >}}


## Tidy up the data

When we do not find the post code or the post town add "no info". Find the ones registered for a particular post code: https://www.doogal.co.uk/MultiplePostcodes.php. In my case everybody in the area TW10 and the neigbourhooding KT2 5 sub area. Sadly, after that, postcode is a bit skattered gun. But nothing stops you from adding postcode street by street. I decided to add the full KT2 5 even though some of the companies will be too far away.  


```python

companies_df['RegAddress.PostCode'].fillna("no info", inplace=True)
companies_df['RegAddress.PostTown'].fillna("no info", inplace=True)

companies_df=companies_df.astype({"CompanyName": 'str',
                                  "RegAddress.AddressLine1": 'str',
                                 "RegAddress.PostTown": 'str',
                                 "RegAddress.PostCode": 'str',
                                 "CompanyCategory" : 'str' ,
                                  "CompanyStatus" : 'str', 
                                 "SICCode.SicText_1" : 'str', 
                                  "SICCode.SicText_2" : 'str',
                                "SICCode.SicText_3" : 'str', 
                                  "SICCode.SicText_4" : 'str'})

TW10companies_df=companies_df.loc[companies_df['RegAddress.PostCode'].str.contains('TW10')|
                                  companies_df['RegAddress.PostCode'].str.contains('KT2 5')|
                                  #companies_df['RegAddress.PostTown'].str.contains('HAM')| #There is alot of HAM
                                  companies_df['RegAddress.PostTown'].str.contains('PETERSHAM')
                                 ]
```

I really only want to know about active companies. Also non-dormant companies. 


```python
TW10_active_companies_df=TW10companies_df.loc[TW10companies_df['CompanyStatus'].str.contains('Active')]
word = "99999"
TW10_active_awake_df=TW10_active_companies_df[TW10_active_companies_df["SICCode.SicText_1"].str.contains(word) == False]
TW10_active_awake_df
```


{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1901</th>
      <td>07201197 LTD</td>
      <td>19 MAY BATE AVENUE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5UR</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>96020 - Hairdressing and other beauty treatment</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>2169</th>
      <td>1 &amp; 2 ARGYLL HOUSE LIMITED</td>
      <td>1 &amp; 2 ARGYLL HOUSE</td>
      <td>RICHMOND</td>
      <td>TW10 7HD</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68320 - Management of real estate on a fee or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3128</th>
      <td>1 SPRING GROVE ROAD LIMITED</td>
      <td>6 SHEEN COMMON DRIVE</td>
      <td>RICHMOND</td>
      <td>TW10 5BN</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4014</th>
      <td>10 CARDIGAN ROAD LIMITED</td>
      <td>10 CARDIGAN ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 6BJ</td>
      <td>PRI/LTD BY GUAR/NSC (Private, limited by guara...</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>5504</th>
      <td>102 CHURCH ROAD LIMITED</td>
      <td>FLAT 1</td>
      <td>RICHMOND</td>
      <td>TW10 6LW</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>98000 - Residents property management</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>4870763</th>
      <td>ZIGGY MOON &amp; SUN LIMITED</td>
      <td>56 TUDOR DRIVE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5QF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>13921 - Manufacture of soft furnishings</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4873906</th>
      <td>ZMS CONSULTANCY LTD</td>
      <td>175 WOLSEY DRIVE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5DR</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>62020 - Information technology consultancy act...</td>
      <td>85600 - Educational support services</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4875348</th>
      <td>ZONCO LIMITED</td>
      <td>26 FALMOUTH HOUSE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5AH</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>11070 - Manufacture of soft drinks; production...</td>
      <td>32500 - Manufacture of medical and dental inst...</td>
      <td>56210 - Event catering activities</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4875397</th>
      <td>ZONE 8 LIMITED</td>
      <td>58 BREAMWATER GARDENS</td>
      <td>SURREY</td>
      <td>TW10 7SH</td>
      <td>Private Limited Company</td>
      <td>Active - Proposal to Strike off</td>
      <td>96090 - Other service activities n.e.c.</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4880027</th>
      <td>£XCELLENT £XCHANGE LIMITED</td>
      <td>48 KINGFISHER DRIVE</td>
      <td>RICHMOND</td>
      <td>TW10 7UE</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>70221 - Financial management</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
  </tbody>
</table>
<p>2443 rows × 10 columns</p>
</div>

{{< /rawhtml >}}

## SIC code

https://resources.companieshouse.gov.uk/sic/

We want to find the companies that are of interest to us. There is 731 different clasifications but the good thing is that they are more or less in numerical order so it we want to look for repair companies for something we cannot repair ourselves:

* 95110	Repair of computers and peripheral equipment
* 95120	Repair of communication equipment
* 95210	Repair of consumer electronics
* 95220	Repair of household appliances and home and garden equipment
* 95230	Repair of footwear and leather goods
* 95240	Repair of furniture and home furnishings
* 95250	Repair of watches, clocks and jewellery
* 95290	Repair of personal and household goods n.e.c.





```python
SIC_repair_TW10_active_companies_df = TW10_active_awake_df.loc[(TW10_active_awake_df['SICCode.SicText_1']>'95105') & (TW10_active_companies_df['SICCode.SicText_1']<'95290')]
SIC_repair_TW10_active_companies_df
```


{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1596373</th>
      <td>FINEMAN ENTERPRISES LTD</td>
      <td>87 HAM STREET</td>
      <td>RICHMOND</td>
      <td>TW10 7HL</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>95110 - Repair of computers and peripheral equ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
  </tbody>
</table>
</div>

{{< /rawhtml >}}

If we want to look into manufactoring anthing textile related:


```python
#14190 - Manufacture of other wearing apparel

SIC_manuFabric_TW10_active_companies_df = TW10_active_awake_df.loc[(TW10_active_awake_df['SICCode.SicText_1']>'14000') & (TW10_active_companies_df['SICCode.SicText_1']<'15000')]
SIC_manuFabric_TW10_active_companies_df
```


{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>190639</th>
      <td>AGA COUTURE LONDON LIMITED</td>
      <td>242 TUDOR DRIVE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5QJ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14132 - Manufacture of other women's outerwear</td>
      <td>74100 - specialised design activities</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>531989</th>
      <td>BEAUTIFULLY MADE FOUNDATION CIC</td>
      <td>20A RICHMOND HILL</td>
      <td>RICHMOND</td>
      <td>TW10 6QX</td>
      <td>Community Interest Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>531990</th>
      <td>BEAUTIFULLY MADE LIMITED</td>
      <td>FLAT 2, 49</td>
      <td>RICHMOND</td>
      <td>TW10 6AW</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3653526</th>
      <td>REVO EUROPE LIMITED</td>
      <td>FLAT 85 THE STAR &amp; GARTER HOUSE</td>
      <td>RICHMOND</td>
      <td>TW10 6BF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3869000</th>
      <td>SEEMEE LONDON LIMITED</td>
      <td>9 HAM STREET</td>
      <td>RICHMOND</td>
      <td>TW10 7HR</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>14390 - Manufacture of other knitted and croch...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3906629</th>
      <td>SHE RISES RESORT LTD</td>
      <td>EVESHAM COURT 14 EVESHAM COURT</td>
      <td>RICHMOND</td>
      <td>TW10 6HJ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4533611</th>
      <td>TUTU PIKIN LIMITED</td>
      <td>COVERDALE COTTAGE</td>
      <td>RICHMOND</td>
      <td>TW10 6EG</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>32990 - Other manufacturing n.e.c.</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
  </tbody>
</table>
</div>

{{< /rawhtml >}}

### Issues with SIC codes

Some companies like to have several codes. But normally this would be secondary activity. So I would want to find out if the companies that I am looking for have it as a secondary thing. 


```python
SIC_14000_TW10_active_df = TW10_active_awake_df.loc[(TW10_active_awake_df['SICCode.SicText_1']>'14000') &
                                                        (TW10_active_awake_df['SICCode.SicText_1']<'15000')|
                                                       (TW10_active_awake_df['SICCode.SicText_2']>'14000') &
                                                        (TW10_active_awake_df['SICCode.SicText_2']<'15000')|
                                                       (TW10_active_awake_df['SICCode.SicText_3']>'14000') &
                                                        (TW10_active_awake_df['SICCode.SicText_3']<'15000')|
                                                       (TW10_active_awake_df['SICCode.SicText_4']>'14000') &
                                                        (TW10_active_awake_df['SICCode.SicText_4']<'15000')
                                                       ]
SIC_14000_TW10_active_df
```


{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>190639</th>
      <td>AGA COUTURE LONDON LIMITED</td>
      <td>242 TUDOR DRIVE</td>
      <td>KINGSTON UPON THAMES</td>
      <td>KT2 5QJ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14132 - Manufacture of other women's outerwear</td>
      <td>74100 - specialised design activities</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>531989</th>
      <td>BEAUTIFULLY MADE FOUNDATION CIC</td>
      <td>20A RICHMOND HILL</td>
      <td>RICHMOND</td>
      <td>TW10 6QX</td>
      <td>Community Interest Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>531990</th>
      <td>BEAUTIFULLY MADE LIMITED</td>
      <td>FLAT 2, 49</td>
      <td>RICHMOND</td>
      <td>TW10 6AW</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3653526</th>
      <td>REVO EUROPE LIMITED</td>
      <td>FLAT 85 THE STAR &amp; GARTER HOUSE</td>
      <td>RICHMOND</td>
      <td>TW10 6BF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3869000</th>
      <td>SEEMEE LONDON LIMITED</td>
      <td>9 HAM STREET</td>
      <td>RICHMOND</td>
      <td>TW10 7HR</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>14390 - Manufacture of other knitted and croch...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3906629</th>
      <td>SHE RISES RESORT LTD</td>
      <td>EVESHAM COURT 14 EVESHAM COURT</td>
      <td>RICHMOND</td>
      <td>TW10 6HJ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4533611</th>
      <td>TUTU PIKIN LIMITED</td>
      <td>COVERDALE COTTAGE</td>
      <td>RICHMOND</td>
      <td>TW10 6EG</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>14190 - Manufacture of other wearing apparel a...</td>
      <td>32990 - Other manufacturing n.e.c.</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
  </tbody>
</table>
</div>

{{< /rawhtml >}}

## Zooming into the data

So, how many companies of each type are there in the neighbourhood?


```python
Q1=TW10_active_awake_df['SICCode.SicText_1'].value_counts()
```


```python
Q1.plot.pie()
plt.ylabel('')
plt.title('SIC code 1');
```

{{< figure src="../LocalCompany/output_22_0.png" >}}



This is too much information even for me! Let us just see the 12 most frequenty type of companies. 


```python
Top_12=TW10_active_awake_df['SICCode.SicText_1'].value_counts().head(12)
Top_12.plot.pie()
#Q1.plot.bar()
plt.ylabel('')
plt.title('SIC code 1');
```
{{< figure src="../LocalCompany/output_24_0.png" >}}



```python
Top_12
```




    70229 - Management consultancy activities other than financial management    185
    62020 - Information technology consultancy activities                        136
    98000 - Residents property management                                        117
    82990 - Other business support service activities n.e.c.                     107
    68209 - Other letting and operating of own or leased real estate              72
    68100 - Buying and selling of own real estate                                 64
    96090 - Other service activities n.e.c.                                       59
    74909 - Other professional, scientific and technical activities n.e.c.        49
    62090 - Other information technology service activities                       47
    62012 - Business and domestic software development                            46
    41100 - Development of building projects                                      41
    47910 - Retail sale via mail order houses or via Internet                     40
    Name: SICCode.SicText_1, dtype: int64



The first dozen of companies represent how much of the total?



```python
Total=TW10_active_awake_df.shape[0]

Top_12_percent=(Top_12.sum()*100)/(Total)
print ('The top 12 percent represents', Top_12_percent.round(), '% of the total', Total)
```

    The top 12 percent represents 39.0 % of the total 2443


Interesting! So, say we have a pub that is for sale called the ham Brewery Tap. Who would be the best companies to partner up with? I would say:

* 68209 - Other letting and operating of own or leased real estate              72
* 68100 - Buying and selling of own real estate                                 64
* 41100 - Development of building projects                                      41

This particular Pub is in TW10 7HT post code as well. 



```python
TW107_df=TW10_active_awake_df.loc[TW10_active_awake_df['RegAddress.PostCode'].str.contains('TW10 7')]

TW107_df_estate=  TW107_df.loc[TW107_df['SICCode.SicText_1'].str.contains('68209')|
                               TW107_df['SICCode.SicText_1'].str.contains('68100')|
                               TW107_df['SICCode.SicText_1'].str.contains('41100')
                                        ]
TW107_df_estate
```


{{< rawhtml >}}

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CompanyName</th>
      <th>RegAddress.AddressLine1</th>
      <th>RegAddress.PostTown</th>
      <th>RegAddress.PostCode</th>
      <th>CompanyCategory</th>
      <th>CompanyStatus</th>
      <th>SICCode.SicText_1</th>
      <th>SICCode.SicText_2</th>
      <th>SICCode.SicText_3</th>
      <th>SICCode.SicText_4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>61352</th>
      <td>63 PEACH STREET LIMITED</td>
      <td>5 SOUTH LODGE</td>
      <td>RICHMOND</td>
      <td>TW10 7JL</td>
      <td>PRI/LTD BY GUAR/NSC (Private, limited by guara...</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>67259</th>
      <td>8 BA INTERNATIONAL LTD</td>
      <td>16 BREAMWATER GARDENS</td>
      <td>RICHMOND</td>
      <td>TW10 7SQ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>41100 - Development of building projects</td>
      <td>56101 - Licensed restaurants</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>284113</th>
      <td>AMEENCO PROPERTY LIMITED</td>
      <td>67 BROUGHTON AVENUE</td>
      <td>RICHMOND</td>
      <td>TW10 7UL</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>68320 - Management of real estate on a fee or ...</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>296400</th>
      <td>AN ORIGIN LTD</td>
      <td>4 VINE COTTAGES</td>
      <td>RICHMOND</td>
      <td>TW10 7AW</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>967009</th>
      <td>CLHM LIMITED</td>
      <td>36 CLIFFORD ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7EA</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1058787</th>
      <td>CORVINUS HOMES LIMITED</td>
      <td>SUITE 5 SOUTH LODGE</td>
      <td>RICHMOND</td>
      <td>TW10 7JL</td>
      <td>Private Limited Company</td>
      <td>Active - Proposal to Strike off</td>
      <td>41100 - Development of building projects</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1286326</th>
      <td>DNA JOINT INVESTMENTS LIMITED</td>
      <td>257 PETERSHAM ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7DA</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1295074</th>
      <td>DOMINO PROPERTY DEVELOPMENTS (2019) LIMITED</td>
      <td>ALL SAINTS CHURCH BUTE AVENUE</td>
      <td>RICHMOND</td>
      <td>TW10 7AX</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1295075</th>
      <td>DOMINO PROPERTY DEVELOPMENTS LIMITED</td>
      <td>ALL SAINTS CHURCH</td>
      <td>PETERSHAM</td>
      <td>TW10 7AX</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1621154</th>
      <td>FLAVORAMA MARKETING LIMITED</td>
      <td>5 LANGHAM GARDENS</td>
      <td>SURREY</td>
      <td>TW10 7LP</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>1709897</th>
      <td>G THULIN PROPERTY COMPANY LTD</td>
      <td>12 WATERMILL CLOSE</td>
      <td>RICHMOND</td>
      <td>TW10 7UH</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>41100 - Development of building projects</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>68320 - Management of real estate on a fee or ...</td>
    </tr>
    <tr>
      <th>2111984</th>
      <td>ICONIC PROPERTY LIMITED</td>
      <td>18 18 LAWRENCE RD</td>
      <td>RICHMOND</td>
      <td>TW10 7LR</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>2521745</th>
      <td>LARK MANAGEMENT LIMITED</td>
      <td>6 HEADWAY CLOSE</td>
      <td>RICHMOND</td>
      <td>TW10 7YW</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>2589736</th>
      <td>LIMONKA LTD</td>
      <td>85 HAM STREET</td>
      <td>RICHMOND</td>
      <td>TW10 7HL</td>
      <td>Private Limited Company</td>
      <td>Active - Proposal to Strike off</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>2726372</th>
      <td>MAGISTER GARDENS LIMITED</td>
      <td>313 PETERSHAM ROAD</td>
      <td>LONDON</td>
      <td>TW10 7DB</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>41100 - Development of building projects</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>2754049</th>
      <td>MANPOWER HUB LTD</td>
      <td>19 MOWBRAY ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7NQ</td>
      <td>Private Limited Company</td>
      <td>Active - Proposal to Strike off</td>
      <td>41100 - Development of building projects</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>68320 - Management of real estate on a fee or ...</td>
    </tr>
    <tr>
      <th>2951562</th>
      <td>MONTROSE VENTURES LIMITED</td>
      <td>MONTROSE HOUSE</td>
      <td>RICHMOND</td>
      <td>TW10 7AD</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>41100 - Development of building projects</td>
      <td>82990 - Other business support service activit...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3331349</th>
      <td>PELHAMSTAR LIMITED</td>
      <td>61 BROUGHTON AVENUE</td>
      <td>RICHMOND</td>
      <td>TW10 7UG</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3355621</th>
      <td>PETERSHAM CAPITAL LIMITED</td>
      <td>10 ASHLEY GARDENS</td>
      <td>RICHMOND</td>
      <td>TW10 7BU</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3555137</th>
      <td>R&amp;B PROPERTY MANAGEMENT LTD</td>
      <td>45 LOCK ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7LQ</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>68320 - Management of real estate on a fee or ...</td>
      <td>98000 - Residents property management</td>
    </tr>
    <tr>
      <th>3750297</th>
      <td>RUGARE PROPERTIES LTD</td>
      <td>9 STRETTON ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7QH</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>3990426</th>
      <td>SMART LONDON PROPERTIES UK LTD</td>
      <td>11 RANDLE ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7LT</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68202 - Letting and operating of conference an...</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>68320 - Management of real estate on a fee or ...</td>
    </tr>
    <tr>
      <th>4079466</th>
      <td>SRISAN PROPERTIES LIMITED</td>
      <td>10 KINGFISHER DRIVE</td>
      <td>RICHMOND</td>
      <td>TW10 7UD</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4106977</th>
      <td>STATE OF THE ARK MUSIC LIMITED</td>
      <td>YEW TREE COTTAGE</td>
      <td>PETERSHAM</td>
      <td>TW10 7AT</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4286112</th>
      <td>THAMES DEVELOPMENTS GROUP LIMITED</td>
      <td>6 FORGE LANE</td>
      <td>RICHMOND</td>
      <td>TW10 7BF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4286452</th>
      <td>THAMES PROPERTIES LTD.</td>
      <td>6 FORGE LANE</td>
      <td>RICHMOND</td>
      <td>TW10 7BF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4286456</th>
      <td>THAMES PROPERTY GROUP LIMITED</td>
      <td>6 FORGE LANE</td>
      <td>RICHMOND</td>
      <td>TW10 7BF</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4336282</th>
      <td>THE HAM POLO CLUB GROUND LIMITED</td>
      <td>THE POLO OFFICE</td>
      <td>PETERSHAM ROAD RICHMOND</td>
      <td>TW10 7AH</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4375120</th>
      <td>THE PYL GROUP LTD</td>
      <td>15 ARLINGTON ROAD</td>
      <td>RICHMOND</td>
      <td>TW10 7BX</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4377966</th>
      <td>THE RESIDENTIAL DEVELOPMENT FUND LIMITED</td>
      <td>PARKLAND HOUSE</td>
      <td>PETERSHAM RICHMOND</td>
      <td>TW10 7AT</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>68100 - Buying and selling of own real estate</td>
      <td>68209 - Other letting and operating of own or ...</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
    <tr>
      <th>4847017</th>
      <td>YOUNGS ROOFING AND BUILDING SOLUTIONS LTD</td>
      <td>50 MAGUIRE DRIVE</td>
      <td>RICHMOND</td>
      <td>TW10 7XY</td>
      <td>Private Limited Company</td>
      <td>Active</td>
      <td>41100 - Development of building projects</td>
      <td>43910 - Roofing activities</td>
      <td>nan</td>
      <td>nan</td>
    </tr>
  </tbody>
</table>
</div>

{{< /rawhtml >}}

Still too many! Maybe if we look at the nearby streets:

* Ham Street
* Evelyn Road
* Back Lane
* Ham Common
* Lock Road
* Lovell Road
* Cleves Road
* The Bench
* Mead Road
* New Road
* Ham Avenue




