+++
date = "2017-08-01T12:00:00-00:00"
title = "civiCRM: the alternative to Costumer Relationship Management"
draft = "False"
tags = ["software", "management", "networks"]
toc = "true"
author = "Andres"
+++

While researching solutions for community groups such as membership management for [Hackspace Foundation](http://hackspace.org.uk/), [South West London Environmental Network](http://swlen.org.uk/), a South London society,  and [Ham United Group](http://hamunitedgroup.org.uk/) I kept coming across a term known as CRM (costumer relationship management). 

CRM are systems or procedures that help you understand your costumers. For example: you send a news letter to a 100 members you might want to know how useful it is to bother to send it. If out of the 100 members, 50 bother to open then email and out of those 20 bother to click on a link and out of those 3 ask questions which you are able to log and track for a possible FAQ and only 1 actually presents themselves as interested in volunteering. Those numbers might change depending on the month. After some time you might realize that actually April is a bad month and that January you can start more activities since there is more engagement. 

As you can imagine this is very interesting for companies to manage their client base. Also you might have raised your eyebrow when I mention how companies know when you press a link or even open an email(*).

There are several of these solutions but I found [civiCRM](http://civicrm.org/) to be the best for many reasons. 

* Being focused on constituents it's language is less commercial: terms like sales, costumer and revenue don't have stronger impact as members, volunteers, impact.
* It is shared on an Free (as in Freedom) Culture licence meaning you do not have to worry about obsolescence that much. 
* Several companies and not just one are invested in its development. 
* It is supported for Wordpress, Joomla and Drupal. Which are well established Content Management Systems (CMS). 
* There is a large community of volunteers willing to help, as well as accredited providers should you want to get it done for you. It is free (as in price) if you are going for a DIY solution
* It is constantly improving, there is now an HR solution, lovely compatible mobile apps, ...
* It is modular: you can slowly add features as you need them. 
* There are frequent conferences all over the world with a large amount of them hosted in London.



(*) If you are interested: do a web search for the following key words: "dead pixel", "email", "tracking".  
 

 
