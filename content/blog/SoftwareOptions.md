+++
date = "2018-01-01T12:00:00-00:00"
title ="Software Alternatives"
author = "Andres"
tags = ["Software", "faif", "opinion"]
toc = "true"
+++


##  Why not?


Sometimes you feel that your tech device (computer, tablet, mobile phone) does it’s own bidding, that there is no other options and you have to use what is offered to you off the shelf or what came with your device. Hopefully this article will show you a completely different alternatives.

The software that I am sharing with you in this article follows these rules or freedoms:
<ol type="a">
  <li>You can run this software for whatever you want. You want to use that sock as a puppet? Be my guest!</li>
  <li>You can study how this software works and change it to do what you want.  You want to use the pillow case to store inside the matching bedsheets? Go ahead!</li>
  <li>You can copy this software so that you can help your neighbour. I also have the recipe for a Spanish omelette, you are welcome to it any time!</li>
  <li>You can also improve this software and share your modifications with the world. You want to put onion in my Spanish omelette recipe? Absolutely not. Just kidding, just do it!</li>
</ol>

Don’t worry if you don’t know how to apply these rules. You can ask a friend ([Richmond MakerLabs](http://richmondmakerlabs.uk/) has many of these friends!), do a web search, or hire someone to do it for you. Note: I did not invent these rules, this is my personal take on the [Free Software Definition](https://en.wikipedia.org/wiki/The_Free_Software_Definition)

## Nearly a dozen options ... 

When starting up my own company I decided to review the tools I would need for my job. So here is a selection of a few items, I have added the website link to the official page to help you with the web search!

1. {{< figure src="/img/postimages/2018-01-01/OpenOffice(TM).png" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/LibreOffice(TM).png" height= "50px" width="auto" >}} **Office Suites**: that is text editors, spreadsheet, slide show editors and diagram editors. I have found using [libreOffice.org](http://libreoffice.org/) has done the trick for me. It is an improved version (see rule d above) of [openOffice.org](https://www.openoffice.org/) that many companies still use. It is fully compatible with your usual proprietary office suites and file formats, plus it has the Draw for flow charts and Database for, well, databases.


2. {{< figure src="/img/postimages/2018-01-01/inkscape-logo(TM).svg.png" height= "50px" width="auto" >}} **Vector Graphic editor**: I have been using [Inkscape.org](http://inkscape.org/) as a vector drawing software. I  used it to design my logo, electrical and electronic circuits and floor plans. Vector drawing is used for when you want images to be seen with the same quality no matter the size: they stretch and do not show pixels. 

3. {{< figure src="/img/postimages/2018-01-01/gimp(tm).png" height= "50px" width="auto" >}} **BitMap editor**: I have been using [gimp.org](http://gimp.org/) as an alternative to the usual photo editing software, same as above. Used it to edit images for my website and a client’s website. But I have also gone a bit more technical for batch editing large amount of images with [imagemagick.org](https://imagemagick.org). More on this on a later post.

4. {{< figure src="/img/postimages/2018-01-01/FreeCAD(tm).png" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/blender(TM).png" height= "50px" width="auto" >}} **Computer aided Design**: I have been using [freeCADweb.org](http://freecadweb.org/) for my 3D engineering drawing. I used for a client that wanted to 3D print some toy components. If I was into 3D renderings or video editing I would use [blender.org](http://blender.org/), but my focus is mainly engineering.

5. {{< figure src="/img/postimages/2018-01-01/GnuCash(tm).png" height= "50px" width="auto" >}} **Accounting**: 
I have been using [gnucash.org](http://gnucash.org) as an alternative to web based accounting systems as a plus, it is installed on your computer so no need to send details over the web and it works off line! It also has a companion phone app. 

6. {{< figure src="/img/postimages/2018-01-01/WordPress(TM).png" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/civiCRM(tm).png" height= "50px" width="auto" >}} **Web content management system**:
I have helped [SWLEN](https://swlen.org.uk/) research some back end stuff on [wordpress.org](https://wordpress.org/) for web pages, for example [CiviCRM.org](https://civicrm.org/) that is a Constituent relationship manager, an alternative to Costumer Relations Management software in the third sector or for business that would like to treat their clients as communities.  

7. {{< figure src="/img/postimages/2018-01-01/Openstreetmap(TM).png" height= "50px" width="auto" >}} **Maps**: For putting locations on maps I have been using openStreetMap.org even details such as trees can be added, useful for the [Petersham Common Woods](https://www.richmond.gov.uk/services/parks_and_open_spaces/find_a_park/petersham_common_woods) who needed a database for their tagged trees. 

8. {{< figure src="/img/postimages/2018-01-01/wrapbootstrap(tm).jpg" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/hugo.png" height= "50px" width="auto" >}} **Website creation**:
For setting up a website I did a [startbootstrap.com](https://startbootstrap.com/) website which is a very simple and basic template that (in my humble opinion) looks quite modern. Plus licence requires no payment and easy to comply with. A similar tool is [gohugo.io](https://gohugo.io/) but this makes website easily editable.

9.  {{< figure src="/img/postimages/2018-01-01/Android_Open_Kang_Project_(AOKP)_(TM).png" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/lineageOS.png" height= "50px" width="auto" >}}
**What about mobile?** Well I used an android operating system called [aokp.co](http://aokp.co/) because *Pink Unicorns Rule!* But there is the main distribution called [Lineageos.org](https://lineageos.org/) so if you dislike the look and feel of your android phone there are many options for you.  Do note that licences and warranties become a bit complicated with mobile phone operating systems. 

10. {{< figure src="/img/postimages/2018-01-01/F-Droid(TM).png" height= "50px" width="auto" >}} {{< figure src="/img/postimages/2018-01-01/k-9(TM).png" height= "50px" width="auto" >}} **Mobile Apps**: store can be changed in Android to [F-Droid.org](https://f-droid.org/) there I found  [k9mail.github.io](https://k9mail.github.io/) my mobile email manager. 
10. {{< figure src="/img/postimages/2018-01-01/NextCloud(tm).png" height= "50px" width="auto" >}} **Cloud storage**: Cloud storage is a tricky one because it is scary, you either use what your company is using, use whatever came with your computer or just what your client uses. If you are like me and you don't like to go with the flow and you are concerned with the big players having your data then [nextcloud.com/](https://nextcloud.com/) is your bet. It is by definition secure and GDPR compliant.

There are many, many more if you want to know just how it applies to your software do get in touch and if I don’t know of an alternative off the top of my head I will find out for you. I am happy to give a short induction if you wish to DIY your way into these packages. 

## About
This article is released under CC-BY-SA 4.0 Andres Muniz-Piniella. Logos shared here are normally trade marked by their owners. 

Part of this article was first released in the **Ham and Petersham Magazine**, Febuary 2018 edition.

Andres is actively involved with the local community via his membership to the Richmond MakerLabs and with local business with his company: C4AD CIC. 

