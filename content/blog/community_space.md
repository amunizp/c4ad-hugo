---
title: "Ham Community Space"
date: 2021-04-10T21:18:32+01:00
draft: false
tags: ["community", "Ham", "networking", "local", "TW10"]
toc: true
author: Andres
---



# Ham Community space
## Background

Some residents of Parkleys started thinking of the idea of having a space to be able to get toguether. We met with representatives of Ham Parade Market and Makers United because we did not want to restrict it to only Parkleys residents. The options concluded from the meeting were:
1. Rent out an existing location. Pro: We get to make it for the purpose we want. Cons: we will need to pay rent to someone that might not have the community at heart. 
2. Book Existing community spaces. Pro: low barrier of entry. Con: will need to be left as multi-use area. 
3. Buy a space outright. Pro: future ensured. Con: high upfront cost: mortgage. 

The conclusion of that meeting was that we needed to make a survey to be able to gauge the interest of our neighbours. We have shared a survey with the local (known) community. 

## Known possible facilities

- Costa Coffee (On Ham Parade).
- Bistro/cafe (on Ham Parade). 
- Richmond Makerlabs (15 min walk from Ham Parade in Ham Close).
- Best and nearest purpose built co-working space Cadbury Works (near Kingston Station, 65 bus stop). 
- www.richmondroadstudios.co.uk
- Ham Library (15 min walk from Ham Parade).
- Ashburnham Road Cafe (opposite the Library)
- Hand and Flowcer Pub (currently closed but on Ham Parade). 
- St. Andrew's Church Hall (10 Min walk from Parade).
- St. Thomas Church Hall (10 Min Walk from Parade). 
- Ham Brewery Tap (currently closed and for sale, 10 min Walk from Parade.). 
- Ham Church Community Hall (20 Min Walk from Parade). 
- St. Richards Community Hall (25 Min Walk from Parade). 
- Garage in Parkleys (5 min Walk from Parade, none for sale or rent at the moment).
- St. Georges Industrial Estate. 

### Discussion about a TW10 booking app

One option that came after the survey was to instead of finding a new facility, to work with existing facilities making it easier to book the online. Key exchange can be done via IOT smart locks. 


## Survey Text
Over the last few months, we have had to adapt to spending more time at home.  Both employees and employers are seeing the pros (and cons) of working from home and an increasing number of people are working freelance or as business owners.  Employers are now suggesting that partial working from home will be offered as be a part of future employment arrangements.  Responding to this change, a group of residents has suggested setting up a community-led space.  It would be a local resource providing space, services, networking, socialising and supporting our health and wellbeing.
## Responses


```python
import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv('Community Space(1-23)PlusHardCopy.csv')
pd.set_option('display.max_rows', None)
pd.set_option('display.max_colwidth', -1)
```

### Would you use a local community-led space (within a 15 min walk)?


```python
Q1=df['Would you use a local community-led space (within a 15 min walk)?'].value_counts()
Q1.plot.pie()
plt.ylabel('')
plt.title('Question1');
```


{{< figure src="../HCS/output_3_0.png" >}}


### What would you like to use it for primarily?


```python
Q2=df['What would you like to use it for primarily?'].value_counts()
Q2.plot.pie()
plt.ylabel('')
plt.title('Question2');
```


{{< figure src="../HCS/output_5_0.png" >}}


Other responses:



```python
Q3=pd.DataFrame(df["If you selected 'other', what other uses would you be interested in?"]).dropna()
Q3
```



{{< table >}}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>If you selected 'other', what other uses would you be interested in?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>All of the above</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Woodworking</td>
    </tr>
    <tr>
      <th>11</th>
      <td>All the above</td>
    </tr>
    <tr>
      <th>12</th>
      <td>All of the above....possibly</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Possibly more than one of above,  also could food and drink be available?</td>
    </tr>
    <tr>
      <th>18</th>
      <td>Indoor cycling</td>
    </tr>
    <tr>
      <th>25</th>
      <td>Hiring meeting space?, Co-working/making space - equipment, printer etc.; Group activities e.g. yoga; Events/Social Space; Other</td>
    </tr>
    <tr>
      <th>26</th>
      <td>Events/Social Space</td>
    </tr>
  </tbody>
</table>
</div>
{{< /table >}}


## In a co-working/networking space, what would you use?  You can select more than one.


```python
col_name4="In a co-working/networking space, what would you use?  You can select more than one."
df.rename(columns={col_name4:'Q_4'}, inplace=True)
q4_df = pd.DataFrame(df['Q_4'])
Q4_df=q4_df.assign(Q_4=q4_df.Q_4.str.split(';')).explode('Q_4')
Q4=Q4_df['Q_4'].value_counts()
Q4.plot.pie()
plt.ylabel('')
plt.title('Question4');
```

{{< figure src="../HCS/output_9_0.png" >}}


## If you selected other, you can provide more information here.


```python
Q5=pd.DataFrame(df["If you selected other, you can provide more information here."]).dropna()
Q5
```



{{< rawhtml >}}
<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>If you selected other, you can provide more information here.</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>It would just be nice to have the option to work somewhere other than my spare room for a change! And I do appreciate how lucky I am to have a spare room.</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Food and drink if possible</td>
    </tr>
    <tr>
      <th>26</th>
      <td>Networking, sharing ideas with your community;Social and wellbeing; Displaying/advertising your service/products</td>
    </tr>
  </tbody>
</table>
</div>
{{< /rawhtml >}}


## Would you, or your employer, pay a weekly or monthly subscription?


```python
Q6=df['Would you, or your employer, pay a weekly or monthly subscription?'].value_counts()
Q6.plot.pie()
plt.ylabel('')
plt.title('Question 6');
```

{{< figure src="../HCS/output_13_0.png" >}}


## Feel free to add any other comments or ideas?


```python
Q7=pd.DataFrame(df["Feel free to add any other comments or ideas?"]).dropna()
Q7
```




{{< rawhtml >}}
<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Feel free to add any other comments or ideas?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>I wish my employer would be willing to pay a subscription!</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Keep me in the loop - not much I can help on/with, but interested to hear any news:</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Retired, but could pay a modest amount. I have no local friends but can drive. I LIVE IN KT3</td>
    </tr>
    <tr>
      <th>8</th>
      <td>A wood &amp; metal working space aka bloqspace</td>
    </tr>
    <tr>
      <th>13</th>
      <td>Makers of different crafts approach me to find them a place/studio where they can create.I strongly believe that it would be beneficial for our community.Here to support if you need me.</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Hope this idea can progress- good luck!</td>
    </tr>
    <tr>
      <th>20</th>
      <td>I am a consultant. I don’t need an office but a co-working space with a specific desk I could rent and use when needed would be great. I need good wifi.</td>
    </tr>
    <tr>
      <th>21</th>
      <td>workshops and stuff.</td>
    </tr>
    <tr>
      <th>22</th>
      <td>I’m definitely interested in being kept in the loop on this idea -sounds like a brilliant initiative. I work in the city and hope my employer will allow flexible working in the future. I love the idea of a space for life beyond work as well - yoga/events and much needed get-togethers! Also, I’m trying to convince some friends who live in east London at the moment to move to Ham - this type of space would definitely appeal to them! Thanks,</td>
    </tr>
    <tr>
      <th>24</th>
      <td>A means of contact concerning the estate.</td>
    </tr>
    <tr>
      <th>25</th>
      <td>A space which could be both meeting and preformaces space would be good. Good Luck!</td>
    </tr>
  </tbody>
</table>
</div>
{{< /rawhtml >}}


## 10 min walk

{{< figure src="../HCS/10MinWalk.png" >}}

