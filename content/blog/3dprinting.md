+++
date = "2018-03-01T12:00:00-00:00"
title = "3D Printing, what for?"
draft = "false"
tags = ["CAD", "fixing", "3D printing"]
toc = "true"
author = "Andres"
+++



I use 3D printing during my work but of course I work in prototyping. What use would it have for individuals in their home? That is before you go out and spend  £300 or more on a new 3D printer. 

## To repair


We were having a picnic with some friends and one of the toys was broken so I decided to having a go at fixing it. 
{{< figure src="../3DPrintingImg/OysterScale.jpg" height= "auto" width="300px" >}}

By using an oyster card for scale I was able to "trace" the contour of the part to the correct size. 

{{< figure src="../3DPrintingImg/InkscapeTrace.png" height= "auto" width="300px" >}}

Then using the dxf or svg file exported you can use a CAD program such as freeCAD. 


{{< figure src="../3DPrintingImg/FreeCADExtrude.png" height= "auto" width="300px" >}}

You feed it into the printer and you have your replacement part:

{{< figure src="../3DPrintingImg/PrintedPart.jpg" height= "auto" width="300px" >}}{{< figure src="../3DPrintingImg/RocketStand.jpg" height= "auto" width="300px" >}}



Do you have a similar broken toy? Not a problem, I have uploaded it to [thingyverse](https://www.thingiverse.com/thing:3110564). You can ask me or somebody else with a 3D printer to print it for you! 


## Tools for the house or office

Some times you want to lock a door and other times you want to have it push open. Sadly when the door closes the door latches automatically and then you can't push it open. What are you tempted to do? Put a door stop. Some times the door stop is a fire extinguisher. Which ironically becomes a fire hazard because your door is meant to close. Or the extinguisher becomes a trip hazard or the air-con/heating is working overtime.

In comes [No-Latch](https://www.thingiverse.com/thing:3105258). This simple device goes into the door frame and stops your door from latching. Want to lock? No problem simply remove it. 

{{< figure src="../3DPrintingImg/no-latch.jpg" height= "auto" width="300px" >}}

There are loads of different things. Do be careful: you might suffer a  case of "I did not know I needed that thing!". 


## Have new toys and/or earrings

How about something for cat lovers? This was done with a thermal PLA. Your body temperature is enough to change this material's colour from purple to pink. 

{{< figure src="../3DPrintingImg/CatThermal.jpg" height= "auto" width="300px" >}}

This is actually a slight modification in size from the one found made by [Joaquin Bugarin](https://www.thingiverse.com/make:525461). 

But also there is other things for fantasy board games. For example this [little figure](https://www.thingiverse.com/thing:1269853)

{{< figure src="../3DPrintingImg/Gargola-01.jpg" height= "auto" width="300 px" >}}

{{< figure src="../3DPrintingImg/Gargola-02.jpg" height= "auto" width="300 px" >}}


